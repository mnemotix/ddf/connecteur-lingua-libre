import sbt._

object Version {
  lazy val scalaVersion = "2.13.8"
  lazy val scalaTest = "3.2.12"
  lazy val scalaLogging = "3.9.5"
  lazy val logback = "1.2.11"
  lazy val synaptix = "3.1.2-SNAPSHOT"
  lazy val rdf4jVersion = "3.7.2"
  lazy val tsConfig = "1.4.1"
  lazy val akkaVersion = "2.6.15"
  lazy val playJson = "2.9.2"
  lazy val akkaStream = "2.6.19"
  lazy val akkaActor = "2.6.19"
  lazy val akkaSlf4J = "2.6.19"
  lazy val analytix = "3.0-SNAPSHOT"
  lazy val alpakkaFile = "3.0.4"

}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val alpakkaFile = "com.lightbend.akka" %% "akka-stream-alpakka-file" % Version.alpakkaFile

  lazy val synaptixCore = "com.mnemotix" % "synaptix-core_2.13" % Version.synaptix
  lazy val synaptixRdfToolkit = "com.mnemotix" % "synaptix-rdf-toolkit_2.13" % Version.synaptix
  lazy val synaptixCacheToolkit = "com.mnemotix" % "synaptix-cache-toolkit_2.13" % Version.synaptix
  lazy val synaptixJobToolkit = "com.mnemotix" % "synaptix-jobs-toolkit_2.13" % Version.synaptix
  lazy val synaptixIndexToolkit ="com.mnemotix" % "synaptix-indexing-toolkit_2.13" % Version.synaptix
  lazy val analytixCore = "com.mnemotix" % "analytix-commons_2.13" % Version.analytix

  lazy val rdf4jRuntime = "org.eclipse.rdf4j" % "rdf4j-runtime" % Version.rdf4jVersion pomOnly()
  lazy val rdf4jSparqlBuilder = "org.eclipse.rdf4j" % "rdf4j-sparqlbuilder" % Version.rdf4jVersion

  lazy val playJson = "com.typesafe.play" %% "play-json" % Version.playJson

  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % Version.akkaActor
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akkaStream
  lazy val akkaSlf4J = "com.typesafe.akka" %% "akka-slf4j" % Version.akkaSlf4J

  lazy val quartz = "org.quartz-scheduler" % "quartz" % "2.3.1"

}