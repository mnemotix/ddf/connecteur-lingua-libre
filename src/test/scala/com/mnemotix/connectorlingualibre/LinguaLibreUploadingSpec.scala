/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.connectorlingualibre

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.connectorlingualibre.LinguaLibreConfig.graphuri
import com.mnemotix.connectorlingualibre.RDFSerializationUtil.createIRI
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, ExecutionContext, Future}

class LinguaLibreUploadingSpec extends AnyFlatSpec with Matchers {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext = ExecutionContext.global

  it should "zip file" in {
    val rdfFilePath = s"${LinguaLibreConfig.homeDir}/lingualibre/rdf/"
    val llUploading = new LinguaLibreUploading(rdfFilesPath = rdfFilePath, contextIRI = Seq(createIRI(graphuri)))

    Await.result(Future.sequence(llUploading.zip()),  5.minutes)
  }

}
