package com.mnemotix.connectorlingualibre

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime

import scala.concurrent.Await

class LinguaLibreConnectorSpec extends AnyFlatSpec with Matchers {

  it should "launch the stream" in {
    Await.result(LinguaLibreConnector.start(), 200.seconds)
    LinguaLibreConnector.shutdown()
  }
}
