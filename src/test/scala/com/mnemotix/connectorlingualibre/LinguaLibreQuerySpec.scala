package com.mnemotix.connectorlingualibre

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class LinguaLibreQuerySpec extends AnyFlatSpec with Matchers {

  val words: List[String] = List("pomme", "orange", "forêt", "géant", "territoire", "garde-manger", "éléphant",
    "immense", "histoire", "lune", "soleil", "gens", "corridor", "couloir", "marcher", "manger", "eau", "pluie",
    "saut", "seau", "fils", "fil", "champ", "même", "pareil", "surprise", "tout le temps", "avant", "après")

  "LLQ" should "handle querying for a single writtenRep" in {
    val time = System.currentTimeMillis()
    words.foreach(word => println(LinguaLibreQuery.selectRecords(word)))
    println("TIME SEPARATED: " + (System.currentTimeMillis() - time))
  }

  "LLQ" should "handle querying multiple writtenRep" in {
    val time = System.currentTimeMillis()
    println(LinguaLibreQuery.selectRecords(words))
    println("TIME COMMON: " + (System.currentTimeMillis() - time))
  }

  it should "handle querying for a single speaker" in {
    println(LinguaLibreQuery.selectSpeakerLocation("https://lingualibre.org/entity/Q547"))
  }

// Some(Speaker(https://lingualibre.org/entity/Q547,Some(Q624805),None,None,Some(Guilhelma)))
  it should "get all writtenRep" in {
    val time = System.currentTimeMillis()
    val writtenReps = LinguaLibreQuery.selectWrittenRepsInFrench()
    println(writtenReps.size)
  }

//Speaker(https://lingualibre.org/entity/Q547,Some(Q624805),None,Some(Cesseras),Some(Guilhelma))
  it should "get geonames info" in {
    val speaker = Speaker("https://lingualibre.org/entity/Q547",Some("Q624805"),None,None,Some("Guilhelma"))
    val speakerResult = WikidataQuery.selectGeonamesId(speaker)
    println(speakerResult) // 
  }
}
