package com.mnemotix.connectorlingualibre

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CommonsQuerySpec extends AnyFlatSpec with Matchers {

  "CQ" should "handle querying for an existing recording" in {
    val record = Record(Option("Q1031"), Option("fond de court"), Option("http://commons.wikimedia.org/wiki/Special:FilePath/LL-Q150%20%28fra%29-Xenoph%C3%B4n-fond%20de%20court.wav"), Option("Q462"),None)
    CommonsQuery.getFileLink(record) shouldBe a [Some[String]]
  }

  it should "handle querying for a non valid link" in {
    val record = Record(Option("Q1031"), Option("fond de court"), Option("http://commons.wikimedia.org/wiki/Special:FilePath/Mwahahah-Q150%20%28fra%29-Xenoph%C3%B4n-fond%20de%20court.wav"), Option("Q462"), None)
    CommonsQuery.getFileLink(record) shouldEqual None
  }
}
