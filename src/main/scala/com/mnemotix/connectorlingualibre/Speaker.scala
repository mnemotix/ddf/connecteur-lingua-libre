package com.mnemotix.connectorlingualibre

import com.mnemotix.connectorlingualibre.RDFSerializationUtil.{createIRI, createLiteral, ddfPrefix, nameSpace, vf}
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.{RDF, RDFS}
import play.api.libs.json.{Json, OFormat}

case class  Speaker(entityId: String, locationWikidataId: Option[String], locationGeonamesId: Option[String], locationName: Option[String], speakerName: Option[String] )

object Speaker {
  implicit lazy val format: OFormat[Speaker] = Json.format[Speaker]

  implicit class toRdfModel(speaker: Speaker) {

    def toRdfModel = {
      val builder = new ModelBuilder()
      val model = nameSpace(builder).namedGraph(s"${LinguaLibreConfig.graphuri}").build()
      val speakerUri = createIRI(speaker.entityId.trim)
      model.add(vf.createStatement(speakerUri, RDF.TYPE, createIRI(s"http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserAccount"), createIRI(s"${LinguaLibreConfig.graphuri}")))
      if (speaker.speakerName.isDefined) {
        model.add(vf.createStatement(speakerUri, RDFS.LABEL, createLiteral(speaker.speakerName.get), createIRI(s"${LinguaLibreConfig.graphuri}")))
      }
      else true
      //todo aiguillé userLocalisation
      if (speaker.locationGeonamesId.isDefined) {
        model.add(vf.createStatement(speakerUri, createIRI(s"${ddfPrefix}userHasLocalisation"), createIRI( s"https://www.geonames.org/${speaker.locationGeonamesId.get}"), createIRI(s"${LinguaLibreConfig.graphuri}")))
        model.add(vf.createStatement(createIRI( s"https://www.geonames.org/${speaker.locationGeonamesId.get}"), RDF.TYPE, createIRI("http://www.w3.org/2004/02/skos/core#Concept"), createIRI(s"${LinguaLibreConfig.graphuri}")))
        model.add(vf.createStatement(createIRI( s"https://www.geonames.org/${speaker.locationGeonamesId.get}"), createIRI("http://www.w3.org/2000/01/rdf-schema#seeAlso"), createIRI( s"https://sws.geonames.org/${speaker.locationGeonamesId.get}"), createIRI(s"${LinguaLibreConfig.graphuri}")))
        if (speaker.locationName.isDefined) model.add(vf.createStatement(createIRI( s"https://www.geonames.org/${speaker.locationGeonamesId.get}"), RDFS.LABEL, createLiteral(speaker.locationName.get), createIRI(s"${LinguaLibreConfig.graphuri}")))
        else true

      }
      else if (speaker.locationWikidataId.isDefined) {
        if (!speaker.locationWikidataId.get.contains("t")) {
          model.add(vf.createStatement(speakerUri, createIRI(s"${ddfPrefix}userHasLocalisation"), createIRI( s"http://www.wikidata.org/entity/${speaker.locationWikidataId.get}"), createIRI(s"${LinguaLibreConfig.graphuri}")))
          model.add(vf.createStatement(createIRI( s"http://www.wikidata.org/entity/${speaker.locationWikidataId.get}"), RDF.TYPE, createIRI("http://www.w3.org/2004/02/skos/core#Concept"), createIRI(s"${LinguaLibreConfig.graphuri}")))
          if (speaker.locationName.isDefined) model.add(vf.createStatement(createIRI( s"http://www.wikidata.org/entity/${speaker.locationWikidataId.get}"), RDFS.LABEL, createLiteral(speaker.locationName.get), createIRI(s"${LinguaLibreConfig.graphuri}")))
          else true
        }
        else true
      }
      else true
      model
    }
  }
}