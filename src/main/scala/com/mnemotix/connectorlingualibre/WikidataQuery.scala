package com.mnemotix.connectorlingualibre

object WikidataQuery extends QueryUtils("https://query.wikidata.org/sparql") {

  def selectGeonamesId(speaker: Speaker): Speaker = {
    if (speaker.locationWikidataId.isDefined) {
      val list = querySelect(s"SELECT ?geonames ?locationName WHERE { OPTIONAL{wd:${speaker.locationWikidataId.get} wdt:P1566 ?geonames } . wd:${speaker.locationWikidataId.get} rdfs:label ?locationName. FILTER(LANG(?locationName) = 'fr') }",
        bindingSet => {
          if (bindingSet.hasBinding("geonames")) {
            speaker.copy(locationGeonamesId = Some(bindingSet.getValue("geonames")).map(_.stringValue()), locationName = Some(bindingSet.getValue("locationName")).map(_.stringValue()))
          }
          else speaker.copy(locationName = Some(bindingSet.getValue("locationName")).map(_.stringValue()))
        }
      )

      if (list.isEmpty) speaker
      else list.head // we assume there's only one speaker object in the list (and there should be!)
    }
    else speaker // wikidataId not defined, so we can't query for geonames
  }
}
