/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.connectorlingualibre

import com.mnemotix.synaptix.cache.RocksDBStore
import play.api.libs.json.Json

import java.io.File
import java.util.concurrent.atomic.AtomicInteger

object LinguaLibreMapper {

  new File(s"${LinguaLibreConfig.homeDir}/lingualibre").mkdirs()
  new File(s"${LinguaLibreConfig.homeDir}/lingualibre/rdf/").mkdirs()

  val tmpFileRecord = new File(LinguaLibreConfig.homeDir + "record.db")
  val storeRecord = new RocksDBStore(tmpFileRecord.getAbsolutePath)

  val tmpFileSpeaker: File = new File(LinguaLibreConfig.homeDir + "speaker.db")
  val storeSpeaker = new RocksDBStore(tmpFileSpeaker.getAbsolutePath)

  storeSpeaker.init
  storeRecord.init()

  val atomicInteger = new AtomicInteger()

  def map() = {
    val it = storeRecord.iterator()
    /*
    while (it.hasNext) {
      val entry = it.next()
      val records = new String(entry.value, "UTF-8").split(storeRecord.RECORD_SEPERATOR).map { s =>
        val js = Json.parse(s)
        js.as[Seq[Record]]
      }
      records.flatten.map(_.toRdfModel)
    }
     */
    val recordsModel = it.map { entry =>
      val records = new String(entry.value, "UTF-8").split(storeRecord.RECORD_SEPERATOR).map { s =>
        val js = Json.parse(s)
        js.as[Seq[Record]]
      }
      records.flatten.map(_.toRdfModel)
    }.flatten

    val it2 = storeSpeaker.iterator()
    val speakerModel = it2.map { entry =>
      val speaker = new String(entry.value, "UTF-8")
      val js = Json.parse(speaker)
      js.as[Speaker].toRdfModel
    }
    /*
    while (it2.hasNext) {
      val entry = it2.next()
      val speaker = new String(entry.value, "UTF-8")
      val js = Json.parse(speaker)
      js.as[Speaker].toRdfModel
    }
     */

    (recordsModel ++ speakerModel).grouped(50000).foreach { models =>
      val merged = RDFSerializationUtil.merge(models)
      RDFSerializationUtil.toFile(merged, s"lingualibre${atomicInteger.addAndGet(1)}")
    }

  }

  def shutdown() = {
    storeSpeaker.shutdown()
    storeRecord.shutdown()
  }
}