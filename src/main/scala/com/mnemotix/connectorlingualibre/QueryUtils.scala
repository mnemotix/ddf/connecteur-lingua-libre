package com.mnemotix.connectorlingualibre

import org.eclipse.rdf4j.query.{BindingSet, TupleQueryResult}
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository

import scala.util.{Failure, Success, Using}
import scala.jdk.CollectionConverters._

class QueryUtils(sparqlEndpoint: String) {
  lazy val repo = new SPARQLRepository(sparqlEndpoint)

  def queryResultToSeq[B](queryResult: TupleQueryResult, acc: List[B] = List(), transform: BindingSet => B): List[B] = {
    if (queryResult.hasNext) {
      queryResultToSeq(queryResult, acc.prepended(transform(queryResult.next())), transform)
    }
    else acc
  }

  // Recursion is fine, but for queries with bigger result sets, we quickly run out of stack space for all the recursive calls.
  def iterativeQueryResultToSeq[B](queryResult: TupleQueryResult, transform: BindingSet => B): Seq[B] = {
    queryResult.iterator().asScala.map(transform).toSeq
  }

  def querySelect[B](query: String, transform: BindingSet => B, iterative: Boolean = false): Seq[B] = {
    Using(repo.getConnection) { conn =>
      if (iterative) iterativeQueryResultToSeq(conn.prepareTupleQuery(query).evaluate(), transform)
      else queryResultToSeq(conn.prepareTupleQuery(query).evaluate(), transform = transform)
    } match {
      case Success(results) => results
      case Failure(exception) =>
        exception.printStackTrace() // TODO: handle exceptions
        /*
         Caused by: org.eclipse.rdf4j.repository.RepositoryException: <html>
            <head><title>502 Bad Gateway</title></head>
         */
        Nil
    }
  }
}
