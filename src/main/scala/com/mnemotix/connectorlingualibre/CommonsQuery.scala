package com.mnemotix.connectorlingualibre


import java.net.{ProxySelector, URI}
import java.net.http.HttpResponse.BodyHandlers
import java.net.http.{HttpClient, HttpRequest}

object CommonsQuery {
  def getFileLink(record: Record): Option[String] = {
    if (record.url.isDefined) {
      val request = HttpRequest.newBuilder()
        .uri(new URI(record.url.get))
        .header("User-Agent", "Lingua-Libre-Connector for Dictionnaire des Francophones <https://gitlab.com/mnemotix/ddf/connecteur-lingua-libre>" )
        .GET()
        .build

      val response = HttpClient.newBuilder()
        .proxy(ProxySelector.getDefault)
        .followRedirects(HttpClient.Redirect.ALWAYS)
        .build()
        .send(request, BodyHandlers.ofString());

      if (response.statusCode() == 404) None // No recordings
      else if (response.statusCode() == 301) Option(response.headers.map().get("location").get(0)) // get the redirection, TODO: make that less prone to NPEs
      else None
    }
    else None
  }
}
