package com.mnemotix.connectorlingualibre

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.synaptix.cache.RocksDBStore
import play.api.libs.json.Json

import java.io.File
import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}

object LinguaLibreConnector {
  implicit val actorSystem: ActorSystem = ActorSystem("lingua-libre-connector")
  implicit val ec: ExecutionContext = ExecutionContext.global

  val tmpFileRecord = new File(LinguaLibreConfig.homeDir + "record.db")
  tmpFileRecord.delete()
  val storeRecord = new RocksDBStore(tmpFileRecord.getAbsolutePath)

  val tmpFileSpeaker: File = new File(LinguaLibreConfig.homeDir + "speaker.db")
  tmpFileSpeaker.delete()
  val storeSpeaker = new RocksDBStore(tmpFileSpeaker.getAbsolutePath)

  storeSpeaker.init()
  storeRecord.init()


  def shutdown() = {
    storeSpeaker.shutdown()
    storeRecord.shutdown()
  }

  // TODO: Config
  val PARALLELISM: Int = 4
  val ELEMENTS_PER_QUERY: Int = 20

  /**
   * Creates a Source out of the writtenReps available in Lingua Libre's database,
   * but without those who already have recordings in the cache.
   */
  private val source: Source[String, NotUsed] = {
    val alreadyRegisteredEntries = storeRecord.iterator().map(ce => ce.key).toSeq
    val writtenRepsInFrench = LinguaLibreQuery.selectWrittenRepsInFrench().map(_.trim)
    Source(writtenRepsInFrench diff alreadyRegisteredEntries)
  }
  /**
   * Queries recordings with batches of writtenReps
   */
  private val flowQueryRecordings: Flow[Seq[String], Seq[Record], NotUsed] =
    Flow[Seq[String]]
      .mapAsyncUnordered(PARALLELISM)(words => Future(LinguaLibreQuery.selectRecords(words)))
  /**
   * Stores recordings data into the cache.
   */
  private val sinkCacheRecordings: Sink[Seq[Record], Future[Done]] =
    Sink.foreach[Seq[Record]](records => {
      records.groupBy(_.writtenRep).filter(_._1.isDefined).foreach[Unit](entry => {
        storeRecord.append(entry._1.get, Json.toJson(entry._2).toString().getBytes(StandardCharsets.UTF_8))
      })
    })

  // Json.toJson(entry._2).toString().getBytes(StandardCharsets.UTF_8)

  /**
   * Extracts speaker ids from a Seq of Records.
   */
  private val flowRecordingsToSpeakersIds: Flow[Seq[Record], Seq[String], NotUsed] =
    Flow[Seq[Record]]
      .map(records => records.map(record => record.speakerId)
        .filter(_.isDefined)
        .map(_.get)
        .distinct
      )
  /**
   * Queries speakers' locations with batches of speaker Ids
   */
  private val flowQuerySpeakersLocation: Flow[Seq[String], Seq[Speaker], NotUsed] =
    Flow[Seq[String]]
      .mapAsyncUnordered(PARALLELISM)(
        speakerIds => Future(LinguaLibreQuery.selectSpeakersLocation(speakerIds))
      )

  /**
   * Filters out speakers' ids that are already stored in the cache.
   */
  private val filterAlreadyRequestedSpeakers: Flow[Seq[String], Seq[String], NotUsed] =
    Flow[Seq[String]]
      .mapAsync(PARALLELISM)(
        s => Future(s.filterNot(speakerId => storeSpeaker.get(speakerId).isDefined))
      )

  /**
   * Stores speakers' data into the cache.
   */
  private val sinkCacheSpeakers: Sink[Seq[Speaker], Future[Done]] =
    Sink.foreach[Seq[Speaker]](speakers => {
      speakers.foreach(speaker => storeSpeaker.put(speaker.entityId, Json.toJson(speaker).toString().getBytes(StandardCharsets.UTF_8)))
    })

  /**
   * Filters out speakers who have no or invalid Wikidata Qid for their location.
   */
  private val filterSpeakersToLocationsIds: Flow[Seq[Speaker], Seq[Speaker], NotUsed] =
    Flow[Seq[Speaker]]
      .mapAsync(PARALLELISM)(s =>
        // only take actual Wikidata Qids, not "t-ids" (which indicate no-value/unknown-value)
        Future(s.filter(speaker => speaker.locationWikidataId.isDefined && speaker.locationWikidataId.get.startsWith("Q")).distinct)
      )

  /**
   * Queries the Geonames Ids of the locations of a batch of Speakers.
   */
  private val flowQueryLocationsGeonamesIds: Flow[Seq[Speaker], Seq[Speaker], NotUsed] =
    Flow[Seq[Speaker]]
      .mapAsync(PARALLELISM)(speakers => {
        Future(speakers.map(speaker => WikidataQuery.selectGeonamesId(speaker)))
      })

  /**
   * Filters out speakers whose Geonames Ids have already been queried.
   */
  private val filterAlreadyRequestedGeonamesIds: Flow[Seq[Speaker], Seq[Speaker], NotUsed] =
    Flow[Seq[Speaker]]
      .mapAsync(PARALLELISM)(
        s => Future(s.filterNot(speaker => speaker.locationGeonamesId.isDefined))
      )

  /**
   * Stream of operations to execute.
   */
  private val stream: Source[Seq[Speaker], NotUsed] = source
    .grouped(ELEMENTS_PER_QUERY)
    .via(flowQueryRecordings)
    .alsoTo(sinkCacheRecordings)
    .via(flowRecordingsToSpeakersIds)
    .via(filterAlreadyRequestedSpeakers)
    .filterNot(seq => seq.isEmpty)
    .via(flowQuerySpeakersLocation)
    .alsoTo(sinkCacheSpeakers)
    .via(filterSpeakersToLocationsIds)
    .via(filterAlreadyRequestedGeonamesIds)
    .filterNot(seq => seq.isEmpty)
    .via(flowQueryLocationsGeonamesIds)
    .alsoTo(sinkCacheSpeakers)

  /**
   * Starts the Stream.
   * @return
   */
  def start(): Future[Done] = {
    val time = System.currentTimeMillis()
    stream
      .run()
      .andThen(_ => println("TIME: " + (System.currentTimeMillis() - time)))
  }

  //TODO: Mécanisme de vérification de la présence des fichiers, si 404 -> retrait du cache, pas besoin de le mettre dans le Flow
}
