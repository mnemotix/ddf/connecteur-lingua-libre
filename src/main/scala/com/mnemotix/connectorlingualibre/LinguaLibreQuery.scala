package com.mnemotix.connectorlingualibre

object LinguaLibreQuery extends QueryUtils("https://lingualibre.org/bigdata/namespace/wdq/sparql") {

  def selectRecords(word: String): Seq[Record] = {
    querySelect(s"""SELECT ?audio ?url ?speaker ?dateCreated ?dateModified
                    |WHERE {
                    |  ?audio prop:P4 entity:Q21.
                    |  ?audio prop:P7 "$word".
                    |  MINUS {?audio prop:P33 [].}
                    |  ?audio prop:P5 ?speaker.
                    |  ?audio prop:P3 ?url.
                    |  ?audio prop:P6 ?dateCreated .
  	                |  OPTIONAL{?audio prop:P31 ?dateModified .}
                    |}""".stripMargin,
      bindingSet => {
        Record(
          entityId = Option(bindingSet.getValue("audio")).map(_.stringValue()),
          writtenRep = Option(word),
          url = Option(bindingSet.getValue("url")).map(_.stringValue()),
          speakerId = Option(bindingSet.getValue("speaker")).map(_.stringValue()),
          dateCreated = Option(bindingSet.getValue("dateCreated")).map(_.stringValue())
          
        )
      }
    )
  }

  def selectRecords(words: Seq[String]): Seq[Record] = {
    if (words.isEmpty) Seq()
    else {
      querySelect(
        s"""SELECT ?word ?audio ?url ?speaker ?dateCreated ?dateModified
           |WHERE {
           |  ?audio prop:P4 entity:Q21.
           |  VALUES ?word { ${words.map(w => s"\"${w.replaceAll(""""""", "")}\"").mkString(" ")} }
           |  ?audio prop:P7 ?word.
           |  MINUS {?audio prop:P33 [].}
           |  ?audio prop:P5 ?speaker.
           |  ?audio prop:P3 ?url.
           |  ?audio prop:P6 ?dateCreated .
           |  OPTIONAL{?audio prop:P31 ?dateModified .}      
           |}""".stripMargin,
        bindingSet => {
          Record(
            entityId = Option(bindingSet.getValue("audio")).map(_.stringValue()),
            writtenRep = Option(bindingSet.getValue("word")).map(_.stringValue()),
            url = Option(bindingSet.getValue("url")).map(_.stringValue()),
            speakerId = Option(bindingSet.getValue("speaker")).map(_.stringValue()),
            dateCreated = Option(bindingSet.getValue("dateCreated")).map(_.stringValue())
          )
        }
      )
    }
  }

  def selectSpeakerLocation(speakerId: String): Option[Speaker] = {
    val list = querySelect(s"SELECT ?location ?speakerName WHERE { <$speakerId> prop:P14 ?location . OPTIONAL{ <$speakerId> prop:P11 ?speakerName }}",
      bindingSet => {
        Speaker(
          entityId = speakerId,
          locationWikidataId = Option(bindingSet.getValue("location")).map(_.stringValue()),
          locationGeonamesId = Option.empty,
          locationName = Option.empty,
          speakerName = Option(bindingSet.getValue("speakerName")).map(_.stringValue())
        )
      }
    )
    if (list.isEmpty) Option.empty
    else Option(list.head) // we assume there's only one speaker object in the list (and there should be!)
  }

  def selectSpeakersLocation(speakersIds: Seq[String]): Seq[Speaker] = {
    if (speakersIds.isEmpty) Seq()
    else {
      querySelect(
        s"""SELECT ?speaker ?speakerName ?location
           |WHERE {
           |  VALUES ?speaker { ${speakersIds.map(sId => s"<$sId>").mkString(" ")} }
           |  ?speaker prop:P14 ?location .
           |  OPTIONAL{ ?speaker prop:P11 ?speakerName .}
           |}
           |""".stripMargin,
        bindingSet => {
          Speaker(
            entityId = bindingSet.getValue("speaker").stringValue(),
            locationWikidataId = Some(bindingSet.getValue("location")).map(_.stringValue()),
            locationGeonamesId = None,
            locationName = None,
            speakerName = Some(bindingSet.getValue("speakerName")).map(_.stringValue())
          )
        }
      )
    }
  }

  def selectWrittenRepsInFrench(): Seq[String] = {
    querySelect("""SELECT DISTINCT ?word
                  |WHERE {
                  |  ?audio prop:P4 entity:Q21 .
                  |  ?audio prop:P7 ?word.
                  |}""".stripMargin,
      _.getValue("word").stringValue(),
      iterative = true)
  }
}
