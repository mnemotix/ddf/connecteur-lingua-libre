/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.connectorlingualibre

import akka.actor.ActorSystem
import com.mnemotix.connectorlingualibre.LinguaLibreConfig.graphuri
import com.mnemotix.connectorlingualibre.RDFSerializationUtil.createIRI
import com.mnemotix.synaptix.core.utils.{RandomNameGenerator, StringUtils}
import com.mnemotix.synaptix.core.{MonitoredService, NotificationValues}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.index.elasticsearch.util.EsListener
import com.typesafe.scalalogging.LazyLogging
import org.quartz.{Job, JobExecutionContext}

import java.io.File
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.reflect.io.Directory
import scala.util.{Failure, Success}

class LinguaLibreJob extends Job with LazyLogging with MonitoredService {

  implicit val executionContext =  ExecutionContext.global

  override def execute(context: JobExecutionContext): Unit = {
    run()
  }

  def run() = {
    lazy val id = RandomNameGenerator.haiku
    implicit lazy val jobSystem: ActorSystem = ActorSystem(id)
    implicit lazy val jobExecutionContext: ExecutionContext = jobSystem.dispatcher

    val esListener = new EsListener(id, "lingualibre", s"${ESConfiguration.prefix.getOrElse("")}synaptixjob", "DDF", Some("import_audio"))
    Await.result(esListener.init(), 1.minute)
    registerListener(s"es-ddf-import-audio", esListener)
    notify(NotificationValues.STARTUP)
    logger.info("Récupération des données lingua libre")
    notify(NotificationValues.UPDATE, Some("Récupération des données lingua libre"))
    Await.result(LinguaLibreConnector.start(), 200.seconds)
    LinguaLibreConnector.shutdown()
    logger.info("Transformation des données lingua libre en RDF")
    notify(NotificationValues.UPDATE, Some("Transformation des données lingua libre en RDF"))
    LinguaLibreMapper.map()
    LinguaLibreMapper.shutdown()
    logger.info("Processus de chargement des données lingua libre")
    notify(NotificationValues.UPDATE, Some("Processus de chargement des données lingua libre"))

    val rdfFilePath = s"${LinguaLibreConfig.homeDir}/lingualibre/rdf/"
    val llUploading = new LinguaLibreUploading(rdfFilesPath = rdfFilePath, contextIRI = Seq(createIRI(graphuri)), Some(esListener))
    val uploading = llUploading.upload()
    Await.result(llUploading.upload(),  30.minutes)


    uploading.onComplete {
      case Success(value) => {
        val path = StringUtils.removeTrailingSlashes(rdfFilePath)
        new Directory(new File(s"${path}/zip/")).deleteRecursively()
        logger.info(s"uploading finished with value = $value")
        notify(NotificationValues.UPDATE, Some("le processus de chargement des fichiers rdf est terminé"))
        notify(NotificationValues.DONE)
        notify(NotificationValues.SHUTDOWN)
      }
      case Failure(e) => {
        logger.error(s"une erreur s'est produite lors du chargement des fichiers", e)
        notifyError(s"une erreur s'est produite lors du chargement des fichiers", Some(e))
        throw new LinguaLibreUploadingException(s"une erreur s'est produite lors du chargement des fichiers", Some(e))
      }
    }
  }

  override val serviceName: String = "LinguaLibreJob"

  override def init(): Unit = {}

  override def shutdown(): Unit = {}
}