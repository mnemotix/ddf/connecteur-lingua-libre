package com.mnemotix.connectorlingualibre

import com.mnemotix.connectorlingualibre.RDFSerializationUtil.{createIRI, createLiteral, ddfPrefix, nameSpace, vf}
import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.{RDF,RDFS}
import play.api.libs.json.{Json, OFormat}

case class Record(entityId: Option[String], writtenRep: Option[String], url: Option[String], speakerId: Option[String], dateCreated: Option[String])

object Record {
  implicit lazy val format: OFormat[Record] = Json.format[Record]


  implicit class toRdfModel(record: Record) {
    def toRdfModel = {
      val builder = new ModelBuilder()
      val model = nameSpace(builder).build()
      val formIRI = createIRI(RDFSerializationUtil.ontolexFormUri(record.writtenRep.get))
      val audioRecordingIRI: IRI = createIRI(record.entityId.get)
      val speakerUri = createIRI(record.speakerId.get)

      model.add(vf.createStatement(formIRI, RDF.TYPE, createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"), createIRI(s"${LinguaLibreConfig.graphuri}")))
      model.add(vf.createStatement(formIRI, createIRI("http://www.w3.org/ns/lemon/ontolex#writtenRep"), createLiteral(record.writtenRep.get), createIRI(s"${LinguaLibreConfig.graphuri}")))
      model.add(vf.createStatement(audioRecordingIRI, RDF.TYPE, createIRI(s"${ddfPrefix}AudioRecording"), createIRI(s"${LinguaLibreConfig.graphuri}")))
      model.add(vf.createStatement(formIRI, createIRI(s"${ddfPrefix}formHasRecording"), audioRecordingIRI, createIRI(s"${LinguaLibreConfig.graphuri}")))
      model.add(vf.createStatement(audioRecordingIRI, createIRI(s"${ddfPrefix}audioFile"), createLiteral(record.url.get), createIRI(s"${LinguaLibreConfig.graphuri}")))

      model.add(vf.createStatement(speakerUri, RDF.TYPE, createIRI(s"http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserAccount"), createIRI(s"${LinguaLibreConfig.graphuri}")))
      model.add(vf.createStatement(audioRecordingIRI, createIRI(s"http://rdfs.org/sioc/ns#has_creator"), speakerUri, createIRI(s"${LinguaLibreConfig.graphuri}")))
      if (record.dateCreated.isDefined) {
        model.add(vf.createStatement(audioRecordingIRI, createIRI(s"http://purl.org/dc/terms/created"), createLiteral(record.dateCreated.get), createIRI(s"${LinguaLibreConfig.graphuri}")))
      }
      else true
      model
    }
  }
}