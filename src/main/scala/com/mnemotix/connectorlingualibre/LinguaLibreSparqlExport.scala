/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.connectorlingualibre

import com.mnemotix.synaptix.rdf.client.RDFClient
import org.eclipse.rdf4j.model.Statement
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.repository.RepositoryResult
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}

import java.io.{File, FileOutputStream}
import java.time.{LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter

class LinguaLibreSparqlExport {

  implicit lazy val conn = RDFClient.getReadConnection(LinguaLibreConfig.repositoryName)
  implicit lazy val writeConn = RDFClient.getWriteConnection(LinguaLibreConfig.repositoryName)

  def shutdown() = {
    conn.close()
    writeConn.close()
  }

  def getContribs(dicoName: String): Unit = {
    lazy val format = "yyyyMMdd"
    lazy val dtf = DateTimeFormatter.ofPattern(format)
    lazy val now = LocalDateTime.now()
    lazy val zone = ZoneId.of("Europe/Paris")
    lazy val dirName = ContribExportConfig.trigOutputDir
    new File(s"${dirName}/${now.format(dtf)}").mkdirs()
    lazy val fileOutputStream = new FileOutputStream(s"${dirName}${now.format(dtf)}/${dicoName}_${now.toEpochSecond(ZoneOffset.of("+02:00")).toString}.trig")
    val graph = SimpleValueFactory.getInstance().createIRI(s"http://data.dictionnairedesfrancophones.org/dict/$dicoName/graph")
    val statements: RepositoryResult[Statement] = conn.conn.getStatements(null, null, null, false, graph)
    Rio.write(statements, fileOutputStream, RDFFormat.TRIG)
  }
}