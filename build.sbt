import Dependencies._
val meta = """META.INF(.)*""".r

lazy val root = (project in file("."))
  .settings(
    name := "connecteur-lingua-libre",
    description := "todo",
    homepage := Some(url("https://gitlab.com/mnemotix/ddf/connecteur-lingua-libre")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/mnemotix/ddf/connecteur-lingua-libre.git"),
        "scm:git@gitlab.com:mnemotix/ddf/connecteur-lingua-libre.git"
      )
    ),
    libraryDependencies ++= Seq(
      scalaTest % Test,
      scalaLogging,
      logbackClassic,
      synaptixCore,
      synaptixRdfToolkit,
      synaptixJobToolkit,
      synaptixCacheToolkit,
      synaptixIndexToolkit,
      rdf4jRuntime,
      rdf4jSparqlBuilder,
      playJson,
      akkaActor,
      akkaStream,
      akkaSlf4J,
      analytixCore,
      alpakkaFile,
      quartz
    ),
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    Compile / mainClass := Some ("com.mnemotix.connectorlingualibre.JobRunner"),
    run / mainClass := Some ("com.mnemotix.connectorlingualibre.JobRunner"),

    assembly / mainClass := Some("com.mnemotix.connectorlingualibre.JobRunner"),
    assembly / assemblyJarName := "ddf-lingua-libre.jar",
    docker / imageNames  := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnemotix/ddf/connecteur-lingua-libre"),
        repository = name.value,
        tag = Some(version.value)
      )
    ),
    docker / buildOptions := BuildOptions(cache = false),
    docker / dockerfile := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("adoptopenjdk/openjdk11:alpine-slim")
        add(artifact, artifactTargetPath)
        run("mkdir", "-p", "/data/")
        run("mkdir", "-p", "/data/upload/")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  ).enablePlugins(DockerPlugin)

