
ThisBuild / scalaVersion := Version.scalaVersion
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"
ThisBuild / licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / developers := List(
  Developer(
    id = "fcuny",
    name = "Florian Cuny",
    email = "florian.cuny@mnemotix.com",
    url = url("http://www.mnemotix.com")
  ),
  Developer(
    id = "prlherisson",
    name = "Pierre-René Lherisson",
    email = "pr.lherisson@mnemotix.com",
    url = url("http://www.mnemotix.com")
  )
)

ThisBuild / useCoursier := false
ThisBuild / onChangedBuildSource := ReloadOnSourceChanges
ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab")

ThisBuild / publishTo := Some("gitlab maven" at "https://gitlab.com/api/v4/projects/37246866/packages/maven")

ThisBuild / resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("public"),
  Resolver.typesafeRepo("releases"),
  Resolver.sbtPluginRepo("releases"),
  "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven"
)